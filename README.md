Update 05.01.2021: New, optimized, debugged and actually really clean version of main code agreement_analysis2.R was provided by affiliate (See affiliation.txt). A newer and final version of raw data can also be found in the main. Nicolas P




The project is composed of 3 main code, of which 1 is the main component. 

The main code is the "agreement_analysis.R" which takes the raw data, attribute for each the SIR value(sensitive, intermediate, Resistant) corresponding to the species and/or treatment. it then uses the newly attributed factor values to creat matrix which help to later attribute and count(unfortunatly manually, see all the "view" function) the number and type of error (minor error(me), Major error(ME), etc...) between the different type of MIC testing protocol(YO for yeast one, MN for Micronaut spectophotometer, VMN for Micronaut visual measurment). 
All in all, the code contains a lot of repetition and manual imput, as i was not able to automatize the process of attributing automoatically the breakpoint and threshold(TRS) valuee. This is mainly the result of each species having specific(if any) value for each treatment, thus making a lot of discreptency between each condition, which was to hard for my level of bioinformatic to implement automatically. 

All the custom function for "agreement_analysis.R" can be found at the end of the file. it is mainly plotting functions for the matrix (one for each MIC testing protocol, for each to have their specific labes and legend). the attribution of SIR values function, and the evaluation of error function(CA function for category agreement).

The second code is the "QC_analysis.R" which as stated analyse a different set of raw data, onyl composed of 3 species. The goal here was to compare the data with themself and the other, which has to be done for each species as they all have different breakpoint values for each treatement and each MIC measurment protocol.

The third code "data_MIC_analysis.R" aim to plot each MIC and species in a diffrent way than the first code. Thus the plotting and prep for said plot are different from the first code. here in the end it is less analytic and more showing the species MIC with their own respective threshold whitin each species and each treatment. Again, due to each treatment for each species and each MIC measurment protocol having diffrent value if any, a lot of repeatition and manual imput can be expected. 

